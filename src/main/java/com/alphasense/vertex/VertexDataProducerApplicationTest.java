package com.alphasense.vertex;

import com.alphasense.vertex.dataobject.Config;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public class VertexDataProducerApplicationTest {

    public static void main(String... args) {

        if (System.getenv("vertex.environment") == null) {
            LambdaHandler lambdaHandler = new LambdaHandler();
            Config config = new Config(
                    "Gene Editing",//"CRISPR",
                    11865,
                    3901,
                    1000,
                    15,
                    "as.vertex.test",
                    "gene"
            );

            lambdaHandler.handleRequest(config, null);
        }
    }
}