package com.alphasense.vertex.controller;

import com.alphasense.vertex.LambdaHandler;
import com.alphasense.vertex.dataobject.Config;
import com.alphasense.vertex.utils.ConfigurationLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 10/04/2016.
 */
@Validated
@RestController
public class StartController {
    private static final Logger logger = LoggerFactory.getLogger(StartController.class);


    @RequestMapping(value = "/lambda", method = RequestMethod.POST)
    public void startLambda(@Valid @RequestBody Config request,
                            final HttpServletRequest httpRequest) {

        logger.info("Received start request with config: {}", request);
        LambdaHandler lambdaHandler = new LambdaHandler();

        if (request == null) {
            List<Config> configList = ConfigurationLoader.getConfigs(System.getenv("vertex.environment"));

            logger.info("Request configuration was not available, scanned following configs from " +
                    "environment folder: {}", configList);

            configList.forEach(config -> lambdaHandler.handleRequest(config, null));
        } else {
            lambdaHandler.handleRequest(request, null);
        }
    }
}