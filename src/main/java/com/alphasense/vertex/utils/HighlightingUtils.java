package com.alphasense.vertex.utils;

import lombok.experimental.UtilityClass;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * @author Niko Himanen
 * Date: 11/08/2017.
 */
@UtilityClass
public final class HighlightingUtils {
    @Nonnull
    public static String markupHits(@Nonnull String text,
                                    @Nonnull List<Integer> startOffsets,
                                    @Nonnull List<Integer> endOffsets,
                                    @Nonnull String hlClass) {
        int hitStart;
        int hitEnd;
        int pos = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0, size = startOffsets.size(); i < size; i++) {
            hitStart = startOffsets.get(i);
            hitEnd = endOffsets.get(i);
            copyCharRange(text, sb, pos, hitStart);
            sb.append(openSpan(hlClass));
            copyCharRange(text, sb, hitStart, hitEnd);
            sb.append(closeSpan());
            pos = hitEnd;
        }
        copyCharRange(text, sb, pos, text.length());
        return sb.toString();
    }

    public static void copyCharRange(@Nonnull String from,
                                     @Nonnull StringBuilder to,
                                     int start,
                                     int end) {
        for (int i = start; i < end; i++) {
            to.append(from.charAt(i));
        }
    }

    public static String openSpan(String hlClass) {
        return "<span class=" + hlClass + '>';
    }

    public static String closeSpan() {
        return "</span>";
    }
}