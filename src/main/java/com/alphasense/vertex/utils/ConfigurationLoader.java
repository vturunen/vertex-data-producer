package com.alphasense.vertex.utils;

import com.alphasense.vertex.dataobject.Config;
import com.google.common.base.Charsets;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Niko Himanen
 *         Date: 10/07/2016.
 */
public class ConfigurationLoader {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationLoader.class);
    private static final Set<String> knownEnvironments = Sets.newHashSet("prod", "test");
    private static final Gson gson = new Gson();

    public static List<Config> getConfigs(String environment) {
        if (!knownEnvironments.contains(environment)) {
            logger.error("Environment: {} is not known", environment);
            return Collections.emptyList();
        }

        List<Config> configs = new LinkedList<>();

        File folder = FileUtils.getFile("src/main/resources/configs/" + environment + "/");
        if (!folder.exists()) {
            logger.error("Configuration folder does not exists in the classpath: {}", folder.getAbsoluteFile());
            return Collections.emptyList();
        }
        for (File file : folder.listFiles()) {
            try {
                configs.add(
                        gson.fromJson(
                                FileUtils.readFileToString(
                                        file,
                                        Charset.forName(Charsets.UTF_8.name())),
                                Config.class)
                );
            } catch (IOException e) {
                logger.error("Could not read config file: {}. Error: {}", file, e);
            }
        }

        return configs;
    }
}