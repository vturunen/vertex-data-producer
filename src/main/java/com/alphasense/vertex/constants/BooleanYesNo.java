package com.alphasense.vertex.constants;

import java.util.Optional;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public enum BooleanYesNo {

    Y(true),
    N(false);

    private final boolean value;

    BooleanYesNo(boolean value) {
        this.value = value;
    }

    private boolean getValue() {
        return value;
    }

    public static Optional<BooleanYesNo> fromBoolean(boolean value) {
        for (BooleanYesNo booleanYesNo : BooleanYesNo.values()) {
            if (booleanYesNo.getValue() == value) {
                return Optional.of(booleanYesNo);
            }
        }

        return Optional.empty();
    }
}