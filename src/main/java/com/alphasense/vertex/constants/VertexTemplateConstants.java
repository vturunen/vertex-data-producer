package com.alphasense.vertex.constants;

/**
 * @author Niko Himanen
 *         Date: 10/03/2016.
 */
public final class VertexTemplateConstants {

    public VertexTemplateConstants() {
        throw new RuntimeException("Class cannot be instantiated!");
    }

    public static final String VERSION = "1.0";
    public static final String COPYRIGHT = "You are receiving this data subject to our standard <a href=\"http://www.alpha-sense.com/terms/#anchordisclaimer\" target=\"_blank\">Disclaimer</a>, <a href=\"http://www.alpha-sense.com/terms/#anchorterms\" target=\"_blank\">Terms of Service</a> and <a href=\"http://www.alpha-sense.com/privacy-policy/\" target=\"_blank\">Privacy Policy</a>.";

    public static final String TITLE_HIGHLIGHT_WRAPPER = "'title-hl'";
    public static final String CONTENTS_HIGHLIGHT_WRAPPER = "'hl'";
}