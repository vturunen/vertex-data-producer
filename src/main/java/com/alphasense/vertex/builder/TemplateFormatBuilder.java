package com.alphasense.vertex.builder;

import com.alphasense.vertex.dataobject.Config;

import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 10/03/2016.
 */
public interface TemplateFormatBuilder<R, P> {

    R build(Config config, List<P> content);
}