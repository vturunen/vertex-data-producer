package com.alphasense.vertex.builder;

import com.alphasense.vertex.dataobject.CompanyFormat;
import com.alphasense.vertex.dataobject.TickerData;
import org.springframework.stereotype.Component;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Component
public class CompanyFormatBuilder {

    public CompanyFormat build(TickerData tickerData) {
        return new CompanyFormat(
                tickerData.getTicker(),
                tickerData.getAsId(),
                tickerData.getIsin(),
                tickerData.getCusip(),
                tickerData.getSedol(),
                tickerData.getCompanyName()
        );
    }
}