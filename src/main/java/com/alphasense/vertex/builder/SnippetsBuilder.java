package com.alphasense.vertex.builder;

import com.alphasense.search.dataobject.response.SEStatement;
import com.alphasense.vertex.utils.HighlightingUtils;
import lombok.experimental.UtilityClass;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Niko Himanen
 * Date: 11/07/2017.
 */
@UtilityClass
public final class SnippetsBuilder {

    @Nonnull
    public static String buildSnippet(@Nonnull SEStatement statement,
                                      @Nonnull String hlWrapper) {
        return buildSnippets(Collections.singletonList(statement), hlWrapper).get(0);
    }

    @Nonnull
    public static List<String> buildSnippets(@Nonnull List<SEStatement> statements,
                                             @Nonnull String hlWrapper) {
        return statements.stream()
                .map(snippet -> buildSnippetFromStatement(snippet, hlWrapper))
                .collect(Collectors.toList());
    }

    @Nonnull
    private static String buildSnippetFromStatement(@Nonnull SEStatement seStatement,
                                                    @Nonnull String hlWrapper) {
        return HighlightingUtils.markupHits(
                seStatement.getText(),
                seStatement.getStartOffsets(),
                seStatement.getEndOffsets(),
                hlWrapper);
    }
}