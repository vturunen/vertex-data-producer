package com.alphasense.vertex.builder;

import com.alphasense.search.dataobject.response.SEClientDocument;
import com.alphasense.vertex.constants.BooleanYesNo;
import com.alphasense.vertex.constants.VertexTemplateConstants;
import com.alphasense.vertex.dataobject.CompanyFormat;
import com.alphasense.vertex.dataobject.TickerData;
import com.alphasense.vertex.dataobject.VertexDocumentFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Component
public class VertexDocumentFormatBuilder {

    @Autowired
    private CompanyFormatBuilder companyFormatBuilder;

    @Nonnull
    public VertexDocumentFormat build(@Nonnull SEClientDocument<?> searchDocument,
                                      @Nonnull List<TickerData> tickerData) {
        List<CompanyFormat> companies = tickerData.stream()
                .map(companyFormatBuilder::build)
                .collect(Collectors.toList());

        List<String> snippets = new ArrayList<>(searchDocument.getContentsSnippets().size() + 1);
        snippets.addAll(
                SnippetsBuilder.buildSnippets(
                        searchDocument.getContentsSnippets(),
                        VertexTemplateConstants.CONTENTS_HIGHLIGHT_WRAPPER
                )
        );

        if (searchDocument.isTitleHit()) {
            snippets.add(
                    SnippetsBuilder.buildSnippet(
                            searchDocument.getTitle(),
                            VertexTemplateConstants.TITLE_HIGHLIGHT_WRAPPER)
                    );
        }

        return new VertexDocumentFormat(
                searchDocument.getId(),
                companies,
                searchDocument.getSeTypes().iterator().next().getName(),
                searchDocument.getDate(),
                searchDocument.getAvailabilityDateTime(),
                BooleanYesNo.fromBoolean(searchDocument.isTitleHit()).orElse(BooleanYesNo.N),
                searchDocument.getNonHighlightedTitle(),
                snippets);
    }
}