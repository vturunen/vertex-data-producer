package com.alphasense.vertex.builder;

import com.alphasense.common.model.ASCountryCode;
import com.alphasense.common.model.DocumentLanguage;
import com.alphasense.search.dataobject.SESortField;
import com.alphasense.search.dataobject.SESortOrder;
import com.alphasense.search.dataobject.constant.*;
import com.alphasense.search.dataobject.filter.SECountryFilter;
import com.alphasense.search.dataobject.filter.SEDateFilter;
import com.alphasense.search.dataobject.filter.SEDocumentLanguageFilter;
import com.alphasense.search.dataobject.filter.doctype.*;
import com.alphasense.search.dataobject.request.SESearchRequest;
import com.alphasense.vertex.dataobject.Config;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.EnumSet;

import static com.alphasense.common.model.ASRegion.GLOB;
import static com.alphasense.search.dataobject.request.SESearchRequest.SearchLocation.DOCUMENT_CONTENT;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Component
public class SearchRequestBuilder {

    public SESearchRequest build(Config config) {
        Instant now = Instant.now();
        Duration hours = Duration.ofHours(config.getTimeRangeInH());
        SESearchRequest seSearchRequest =
                new SESearchRequest()
                        .withKeyword(config.getSearchQuery())
                        .withSlop(config.getSlop())
                        .withUserDataFilter(config.getCompanyId(), config.getUserId())
                        .withDateFilter(new SEDateFilter(now.minus(hours), now));
        setDefaultParams(seSearchRequest);
        return seSearchRequest;
    }

    private void setDefaultParams(SESearchRequest seSearchRequest) {
        seSearchRequest
                .withHighlightedContents()
                .withHighlightedTitle()
                .withSortField(SESortField.DATE)
                .withSortOrder(SESortOrder.DESC)
                .withSearchLocations(EnumSet.of(DOCUMENT_CONTENT))
                .withPressDocumentTypeFilter(new SEPressDocumentTypeFilter(EnumSet.of(SEPressDocumentType.ALL), GLOB))
                .withPresentationDocumentTypeFilter(new SEPresentationDocumentTypeFilter(EnumSet.allOf(SEPresentationDocumentType.class), GLOB))
                .withTranscriptDocumentTypeFilter(new SETranscriptDocumentTypeFilter(EnumSet.allOf(SETranscriptDocumentType.class), GLOB))
                .withWebDocumentTypeFilter(new SEWebDocumentTypeFilter(EnumSet.allOf(SEWebDocumentType.class), GLOB))
                .withDocumentLanguageFilter(new SEDocumentLanguageFilter(true, EnumSet.of(DocumentLanguage.EN)))
                .withGlobalDocumentTypeFilter(new SEGlobalDocumentTypeFilter(EnumSet.allOf(SEGlobalDocumentType.class), GLOB))
                .withSecDocumentTypeFilter(new SESecDocumentTypeFilter(EnumSet.allOf(SESecDocumentType.class), GLOB))
                .withCountryFilter(new SECountryFilter(EnumSet.allOf(ASCountryCode.class)));
    }
}