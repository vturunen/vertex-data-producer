package com.alphasense.vertex.builder;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Niko Himanen
 *         Date: 09/29/2016.
 */
@Component
public class FileNameBuilder {

    private final DateTimeFormatter dateFormatter;

    public FileNameBuilder() {
        this.dateFormatter =
                DateTimeFormatter.ISO_DATE;
    }

    public String build(String filePrefix) {
        LocalDateTime timeNow = LocalDateTime.now();
        return dateFormatter.format(timeNow) +
                "/" +
                filePrefix + "_" +
                Instant.now().getEpochSecond();
    }
}