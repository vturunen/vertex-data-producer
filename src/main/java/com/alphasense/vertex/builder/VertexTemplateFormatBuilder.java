package com.alphasense.vertex.builder;

import com.alphasense.vertex.constants.VertexTemplateConstants;
import com.alphasense.vertex.dataobject.Config;
import com.alphasense.vertex.dataobject.VertexDocumentFormat;
import com.alphasense.vertex.dataobject.VertexTemplateFormat;

import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 10/03/2016.
 */
public class VertexTemplateFormatBuilder
        implements TemplateFormatBuilder<VertexTemplateFormat, VertexDocumentFormat> {

    public VertexTemplateFormat build(Config config, List<VertexDocumentFormat> documentFormats) {
        return new VertexTemplateFormat(
                config.getSearchQuery(),
                VertexTemplateConstants.VERSION,
                VertexTemplateConstants.COPYRIGHT,
                documentFormats);
    }
}