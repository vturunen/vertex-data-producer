package com.alphasense.vertex.repository;

import com.alphasense.vertex.dataobject.TickerData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public interface BETickerRepository extends JpaRepository<TickerData, Long> {

    @Query("SELECT ticker FROM TickerData ticker " +
            "WHERE ticker.asId IN (:asIds) " +
            "AND   ticker.displayTicker = 1 " +
            "AND   ticker.filingsExist = 1")
    List<TickerData> findTickerDataForAsId(@Param("asIds") List<String> asIds);
}