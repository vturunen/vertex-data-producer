package com.alphasense.vertex.service.s3;

import com.alphasense.vertex.builder.FileNameBuilder;
import com.alphasense.vertex.builder.TemplateFormatBuilder;
import com.alphasense.vertex.dataobject.Config;
import com.alphasense.vertex.service.JsonExporter;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public class S3DataExporter extends JsonExporter {

    private static final Logger logger = LoggerFactory.getLogger(S3DataExporter.class);

    private final FileNameBuilder fileNameBuilder;
    private final String bucketName;
    private final String filePrefix;

    private final AmazonS3 storageClient;

    public S3DataExporter(Config config,
                          AmazonS3 client,
                          TemplateFormatBuilder templateFormatBuilder) {
        super(templateFormatBuilder, config);
        this.bucketName = config.getBucketName();
        this.filePrefix = config.getFilePrefix();
        this.storageClient = client;
        this.fileNameBuilder = new FileNameBuilder();
    }

    @Override
    public void exportBatch(String resultInJson) {

        logger.info("Exporting data to S3 bucket: {}", bucketName);
        logger.debug("Data to export: {}", resultInJson);

        try {
            if (!storageClient.doesBucketExist(bucketName)) {
                logger.warn("Bucket {} does not exists. Does not export.", bucketName);
                return;
            }

            String fileName = fileNameBuilder.build(filePrefix);

            logger.info("Storing data with file name: {}", fileName);

            PutObjectResult putObjectResult = storageClient.putObject(
                    bucketName,
                    fileName,
                    resultInJson
            );
            logger.info("Data exported to S3 with result: {}", putObjectResult);
        } catch (AmazonServiceException e) {
            logger.error("Exception when storing to S3", e);
        }
    }
}