package com.alphasense.vertex.service;

import com.alphasense.vertex.builder.TemplateFormatBuilder;
import com.alphasense.vertex.dataobject.Config;
import com.google.gson.Gson;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public abstract class JsonExporter<T> implements DataExporter<T> {

    private static final Gson gson = new Gson();

    private final List<T> exportableData;
    private final TemplateFormatBuilder templateFormatBuilder;
    private final Config config;

    protected JsonExporter(TemplateFormatBuilder templateFormatBuilder,
                           Config config) {
        this.exportableData = new LinkedList<>();
        this.templateFormatBuilder = templateFormatBuilder;
        this.config = config;
    }

    @Override
    public void exportData(T data) {
        exportableData.add(data);
    }

    @Override
    public void flushData() {
        exportBatch(
                gson.toJson(templateFormatBuilder.build(config, exportableData))
        );
        exportableData.isEmpty();
    }

    protected abstract void exportBatch(String resultInJson);
}