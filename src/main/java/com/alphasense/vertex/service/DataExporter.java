package com.alphasense.vertex.service;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
interface DataExporter<T> {

    void exportData(T data);

    void flushData();
}