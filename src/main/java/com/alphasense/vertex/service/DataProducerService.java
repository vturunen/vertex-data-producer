package com.alphasense.vertex.service;

import com.alphasense.vertex.builder.VertexTemplateFormatBuilder;
import com.alphasense.vertex.dataobject.Config;
import com.alphasense.vertex.service.s3.S3DataExporter;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Service
public class DataProducerService {

    @Autowired
    private SearchExportingService searchExportingService;

    @Autowired
    private AmazonS3 s3Client;

    public void executeSearch(Config config) {
        searchExportingService.searchDocuments(
                config,
                new S3DataExporter(
                        config,
                        s3Client,
                        new VertexTemplateFormatBuilder()
                )
        );
    }
}