package com.alphasense.vertex.service;

import com.alphasense.search.client.SELibError;
import com.alphasense.search.client.SEMultiResponseCallback;
import com.alphasense.search.client.SESearchDocumentService;
import com.alphasense.search.dataobject.response.SESearchCompleteResponse;
import com.alphasense.search.dataobject.response.SESearchResponse;
import com.alphasense.vertex.builder.SearchRequestBuilder;
import com.alphasense.vertex.builder.VertexDocumentFormatBuilder;
import com.alphasense.vertex.dataobject.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Service
public class SearchExportingService {

    private static final Logger logger = LoggerFactory.getLogger(SearchExportingService.class);

    @Value("${search.timeoutInMs}")
    public static long TIMEOUT_IN_MS = 300000L;

    @Autowired
    private SESearchDocumentService seSearchDocumentService;

    @Autowired
    private VertexDocumentFormatBuilder vertexDocumentFormatBuilder;

    @Autowired
    private SearchRequestBuilder searchRequestBuilder;

    @Autowired
    private TickerDataService tickerDataService;

    public void searchDocuments(Config config, DataExporter dataExporter) {
        logger.info("Searching for docs with config: {}", config);

        CountDownLatch latch = new CountDownLatch(1);
        ResponseHandler responseHandler =
                new ResponseHandler(
                        dataExporter,
                        vertexDocumentFormatBuilder,
                        tickerDataService,
                        latch
                );

        seSearchDocumentService.search(
                searchRequestBuilder.build(config),
                responseHandler
        );

        try {
            logger.info("Waiting search result for {} milliseconds.", TIMEOUT_IN_MS);
            latch.await(TIMEOUT_IN_MS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.error("Interrupted while waiting response from search.");
        }

        if (responseHandler.wasError()) {
            throw new RuntimeException("Encountered error during search");
        }
    }

    private static class ResponseHandler
            implements SEMultiResponseCallback<SESearchResponse, SESearchCompleteResponse> {

        private static final Logger logger = LoggerFactory.getLogger(ResponseHandler.class);
        private final CountDownLatch latch;
        private final DataExporter dataExporter;
        private final VertexDocumentFormatBuilder vertexDocumentFormatBuilder;
        private final TickerDataService tickerDataService;
        private volatile boolean encounteredError = false;

        public ResponseHandler(DataExporter dataExporter,
                               VertexDocumentFormatBuilder vertexDocumentFormatBuilder,
                               TickerDataService tickerDataService,
                               CountDownLatch latch) {
            this.dataExporter = dataExporter;
            this.vertexDocumentFormatBuilder = vertexDocumentFormatBuilder;
            this.tickerDataService = tickerDataService;
            this.latch = latch;
        }

        @Override
        public void onContinue(SESearchResponse seSearchResponse) {
            logger.info("Exporting document batch {}.", seSearchResponse);
            seSearchResponse.getDocuments().forEach(
                    document -> dataExporter.exportData(
                            vertexDocumentFormatBuilder.build(
                                    document,
                                    tickerDataService.getTickerDataForAsIds(document.getAsIds())
                            )
                    )
            );
        }

        @Override
        public void onFinish(SESearchCompleteResponse seSearchCompleteResponse) {
            try {
                logger.info("All results fetched, flushing data {}.", seSearchCompleteResponse);
                dataExporter.flushData();
            } finally {
                latch.countDown();
            }
        }

        @Override
        public void onError(SELibError seError) {
            try {
                logger.error("Error during search! {}. Trying to flush data.", seError);
                dataExporter.flushData();
                encounteredError = true;
            } finally {
                latch.countDown();
            }
        }

        public boolean wasError() {
            return encounteredError;
        }
    }
}