package com.alphasense.vertex.service;

import com.alphasense.vertex.dataobject.TickerData;
import com.alphasense.vertex.repository.BETickerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Service
public class TickerDataService {

    @Autowired
    private BETickerRepository repository;

    public List<TickerData> getTickerDataForAsIds(List<String> asIds) {
        return repository.findTickerDataForAsId(asIds);
    }
}