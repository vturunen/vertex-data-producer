package com.alphasense.vertex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Niko Himanen
 *         Date: 10/04/2016.
 */
@SpringBootApplication
public class SpringBootVertex {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootVertex.class, args);
    }
}