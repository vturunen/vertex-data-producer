package com.alphasense.vertex;

import com.alphasense.vertex.config.SpringConfig;
import com.alphasense.vertex.dataobject.Config;
import com.alphasense.vertex.service.DataProducerService;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
public class LambdaHandler
        extends AbstractHandler<SpringConfig>
        implements RequestHandler<Config, Void> {

    private static final Logger logger = LoggerFactory.getLogger(LambdaHandler.class);

    @Override
    public Void handleRequest(Config input, Context context) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Config>> problems = validator.validate(input);
        if (!problems.isEmpty()) {
            throw new RuntimeException(problems.toString());
        }
        logger.info("Starting vertex search with parameters: {}", input);
        DataProducerService dataProducerService = getApplicationContext().getBean(DataProducerService.class);

        dataProducerService.executeSearch(input);

        logger.info("Vertex search executed and result stored.");
        return null;
    }
}