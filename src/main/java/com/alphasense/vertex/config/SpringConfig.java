package com.alphasense.vertex.config;

import com.alphasense.search.client.SESearchDocumentService;
import com.alphasense.search.client.SearchDocumentServiceImpl;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Configuration
@ComponentScan({"com.alphasense.vertex"})
@Import(com.alphasense.search.SearchApplication.class)
public class SpringConfig {

    @Bean
    public SESearchDocumentService seSearchDocumentService() {
        return new SearchDocumentServiceImpl();
    }

    @Bean
    public AmazonS3 amazonS3Client() {
        return AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_1).build();
    }
}