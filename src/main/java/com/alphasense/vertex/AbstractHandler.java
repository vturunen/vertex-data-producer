package com.alphasense.vertex;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.ParameterizedType;

/**
 * @author Niko Himanen
 *         Date: 18/08/16.
 */
abstract class AbstractHandler<T> {

    private ApplicationContext applicationContext;

    AbstractHandler() {
        Class typeParameterClass = (Class) ((ParameterizedType) getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];

        if (!typeParameterClass.isAnnotationPresent(Configuration.class)) {
            throw new RuntimeException(typeParameterClass + " is not a @Configuration class");
        }

        applicationContext = new AnnotationConfigApplicationContext(typeParameterClass);
    }

    /**
     * Use this getter to access to Spring Application Context
     *
     * @return ApplicationContext
     */
    ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}