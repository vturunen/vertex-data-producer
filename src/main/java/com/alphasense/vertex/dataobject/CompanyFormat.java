package com.alphasense.vertex.dataobject;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Getter
@AllArgsConstructor
@ToString
public class CompanyFormat {
    @SerializedName("AS_TICKER")
    private String asTicker;

    @SerializedName("ASID")
    private String asId;

    @SerializedName("ISIN")
    private String isin;

    @SerializedName("CUSIP")
    private String cusip;

    @SerializedName("SEDOL")
    private String sedol;

    @SerializedName("COMPANY_NAME")
    private String companyName;
}