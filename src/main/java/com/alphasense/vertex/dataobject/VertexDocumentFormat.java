package com.alphasense.vertex.dataobject;

import com.alphasense.vertex.constants.BooleanYesNo;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Getter
@AllArgsConstructor
@ToString
public class VertexDocumentFormat {

    @SerializedName("DOC_ID")
    private String docId;

    @SerializedName("companies")
    private List<CompanyFormat> companies;

    @SerializedName("SOURCE")
    private String source;

    @SerializedName("RELEASED_DATE")
    private Date releaseDate;

    @SerializedName("AVAILABILITY_DATE")
    private Date availabilityDate;

    @SerializedName("TITLE_HIT_YN")
    private BooleanYesNo titleHit;

    @SerializedName("TITLE")
    private String title;

    @SerializedName("snippets")
    private List<String> snippets;
}