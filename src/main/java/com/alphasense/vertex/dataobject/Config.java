package com.alphasense.vertex.dataobject;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Config {
    @NotEmpty(message = "searchQuery must not be empty")
    private String searchQuery;
    @NotNull(message = "UserId must be available")
    private Integer userId;
    @NotNull(message = "companyId must be available")
    private Integer companyId;
    @Min(value = 1, message = "timeRangeInH must be at least 1 hour")
    @NotNull(message = "timeRangeInH must be available")
    private Integer timeRangeInH;
    @NotNull(message = "slop must be available")
    private Integer slop;
    @NotNull(message = "bucketName must be available")
    private String bucketName;
    @NotNull(message = "filePrefix must be available")
    private String filePrefix;
}