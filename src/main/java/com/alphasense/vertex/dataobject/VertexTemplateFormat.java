package com.alphasense.vertex.dataobject;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

/**
 * @author Niko Himanen
 *         Date: 10/03/2016.
 */
@Getter
@AllArgsConstructor
@ToString
public class VertexTemplateFormat {

    @SerializedName("KEYWORD")
    private String keywords;

    @SerializedName("VERSION")
    private String version;

    @SerializedName("COPYRIGHT")
    private String copyright;

    @SerializedName("DOCUMENTS")
    private List<VertexDocumentFormat> vertexDocumentFormatFormats;
}