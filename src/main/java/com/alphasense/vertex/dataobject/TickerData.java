package com.alphasense.vertex.dataobject;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity
@Table(name = "master_ticker")
public class TickerData {

    @Id
    @Column(name = "ticker_id")
    private Long tickerId;

    @Column(name = "ticker")
    private String ticker;

    @Column(name = "as_id")
    private String asId;

    @Column(name = "isin")
    private String isin;

    @Column(name = "cusip")
    private String cusip;

    @Column(name = "sedol")
    private String sedol;

    @Column(name = "display_ticker")
    private Boolean displayTicker;

    @Column(name = "filings_exist")
    private Boolean filingsExist;

    @Column(name = "company_name")
    private String companyName;
}