package com.alphasense.vertex.service.s3;

import com.alphasense.vertex.SpringComponentTest;
import com.alphasense.vertex.builder.VertexTemplateFormatBuilder;
import com.alphasense.vertex.dataobject.Config;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.gson.Gson;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Niko Himanen
 *         Date: 09/30/2016.
 */
public class S3DataExporterTest extends SpringComponentTest {

    private static final Gson gson = new Gson();

    @Mock
    private AmazonS3Client s3Client;

    @Test
    public void exportBatch() throws Exception {
        S3DataExporter dataExporter =
                new S3DataExporter(
                        new Config(
                                "query",
                                123,
                                1,
                                24,
                                15,
                                "bucketName",
                                "prefix"),
                        s3Client,
                        new VertexTemplateFormatBuilder()
                );
        when(s3Client.doesBucketExist(anyString())).thenReturn(true);
        dataExporter.exportBatch(gson.toJson("thisIsTestData"));
    }
}