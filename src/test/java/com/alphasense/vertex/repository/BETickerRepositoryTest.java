package com.alphasense.vertex.repository;

import com.alphasense.vertex.SpringComponentTest;
import com.alphasense.vertex.dataobject.TickerData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Niko Himanen
 *         Date: 09/29/2016.
 */
public class BETickerRepositoryTest extends SpringComponentTest {

    @Autowired
    private BETickerRepository repository;

    @Test
    public void findTickerDataForAsId() throws Exception {
        List<String> asIds = Collections.singletonList("TK91310");
        List<TickerData> tickers = repository.findTickerDataForAsId(asIds);

        assertEquals(1, tickers.size());
    }
}