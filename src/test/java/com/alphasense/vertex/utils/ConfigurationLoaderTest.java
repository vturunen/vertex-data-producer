package com.alphasense.vertex.utils;

import com.alphasense.vertex.SpringComponentTest;
import com.alphasense.vertex.dataobject.Config;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Niko Himanen
 *         Date: 10/07/2016.
 */
public class ConfigurationLoaderTest extends SpringComponentTest {

    @Test
    public void getConfigs() throws Exception {
        List<Config> configs = ConfigurationLoader.getConfigs("test");

        assertEquals(3, configs.size());
    }
}