package com.alphasense.vertex.builder;

import com.alphasense.search.dataobject.constant.SEDjnDocumentType;
import com.alphasense.search.dataobject.response.SEClientDocument;
import com.alphasense.search.dataobject.response.SEStatement;
import com.alphasense.vertex.SpringComponentTest;
import com.alphasense.vertex.constants.BooleanYesNo;
import com.alphasense.vertex.dataobject.TickerData;
import com.alphasense.vertex.dataobject.VertexDocumentFormat;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author Niko Himanen
 *         Date: 09/29/2016.
 */
public class VertexDocumentFormatBuilderTest extends SpringComponentTest {

    @Autowired
    private VertexDocumentFormatBuilder vertexDocumentFormatBuilder;

    @Mock
    private SEClientDocument clientDocument;

    @Mock
    private TickerData tickerData;

    @Mock
    private CompanyFormatBuilder companyFormatBuilder;

    @Test
    public void build() throws Exception {

        String doId = "docId";
        String asId = "asId";
        String isin = "isin";
        String cusip = "cusip";
        String sedol = "sedol";
        SEDjnDocumentType source = SEDjnDocumentType.ALL;
        Date dateNow = new Date(Instant.now().toEpochMilli());
        List<SEStatement> statements =
                Collections.singletonList(
                        SEStatement.builder()
                                .text("thisIsSnippet")
                                .startOffsets(Collections.singletonList(4))
                                .endOffsets(Collections.singletonList(6))
                                .build()
        );

        List<String> snippets = Collections.singletonList("this<span class='hl'>Is</span>Snippet");

        when(clientDocument.getId()).thenReturn(doId);
        when(clientDocument.getSeTypes()).thenReturn(Collections.singleton(source));
        when(clientDocument.getDate()).thenReturn(dateNow);
        when(clientDocument.getAvailabilityDateTime()).thenReturn(dateNow);
        when(clientDocument.isTitleHit()).thenReturn(false);
        when(clientDocument.getContentsSnippets()).thenReturn(statements);

        when(tickerData.getAsId()).thenReturn(asId);
        when(tickerData.getIsin()).thenReturn(isin);
        when(tickerData.getCusip()).thenReturn(cusip);
        when(tickerData.getSedol()).thenReturn(sedol);

        List<TickerData> tickerDatas = Collections.singletonList(tickerData);

        VertexDocumentFormat vertexDocumentFormat = vertexDocumentFormatBuilder.build(clientDocument, tickerDatas);

        assertEquals(doId, vertexDocumentFormat.getDocId());
        assertEquals(source.getName(), vertexDocumentFormat.getSource());
        assertEquals(dateNow, vertexDocumentFormat.getAvailabilityDate());
        assertEquals(dateNow, vertexDocumentFormat.getReleaseDate());
        assertEquals(BooleanYesNo.N, vertexDocumentFormat.getTitleHit());
        assertEquals(snippets, vertexDocumentFormat.getSnippets());
    }
}