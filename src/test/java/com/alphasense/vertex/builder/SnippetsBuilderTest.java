package com.alphasense.vertex.builder;

import com.alphasense.search.dataobject.response.SEStatement;
import org.assertj.core.util.Strings;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.alphasense.vertex.constants.VertexTemplateConstants.CONTENTS_HIGHLIGHT_WRAPPER;
import static com.alphasense.vertex.constants.VertexTemplateConstants.TITLE_HIGHLIGHT_WRAPPER;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

/**
 * @author Niko Himanen
 * Date: 11/07/2017.
 */
public class SnippetsBuilderTest {

    private static final String CONTENT_TEXT = "this is content";

    @Test
    public void buildSnippet_single() throws Exception {
        SEStatement contentStatement = getSingleHLStatement();
        String snippet =
                SnippetsBuilder.buildSnippet(
                        contentStatement, CONTENTS_HIGHLIGHT_WRAPPER);

        assertEquals(
                Strings.formatIfArgs(
                        "this <span class=%s>is</span> content",
                        CONTENTS_HIGHLIGHT_WRAPPER),
                snippet
        );
    }

    @Test
    public void buildSnippet_multiHl() throws Exception {
        SEStatement contentStatement = getMultiHLStatement();
        String snippet =
                SnippetsBuilder.buildSnippet(
                        contentStatement, CONTENTS_HIGHLIGHT_WRAPPER);

        assertEquals(
                Strings.formatIfArgs(
                        "this <span class=%s>is</span> <span class=%s>content</span>",
                        CONTENTS_HIGHLIGHT_WRAPPER,
                        CONTENTS_HIGHLIGHT_WRAPPER),
                snippet
        );
    }

    @Test
    public void buildSnippets() throws Exception {
        List<SEStatement> statements = Arrays.asList(
                getSingleHLStatement(),
                getMultiHLStatement()
        );

        List<String> snippets = SnippetsBuilder.buildSnippets(statements, TITLE_HIGHLIGHT_WRAPPER);

        assertEquals(2, snippets.size());

        assertTrue(snippets.contains(
                Strings.formatIfArgs(
                "this <span class=%s>is</span> content",
                         TITLE_HIGHLIGHT_WRAPPER))
        );

        assertTrue(snippets.contains(
                Strings.formatIfArgs(
                        "this <span class=%s>is</span> <span class=%s>content</span>",
                        TITLE_HIGHLIGHT_WRAPPER,
                        TITLE_HIGHLIGHT_WRAPPER))
        );
    }

    private SEStatement getSingleHLStatement() {
        return SEStatement.builder()
                .text(CONTENT_TEXT)
                .startOffsets(singletonList(5))
                .endOffsets(singletonList(7))
                .build();
    }

    private SEStatement getMultiHLStatement() {
        return SEStatement.builder()
                .text(CONTENT_TEXT)
                .startOffsets(asList(5, 8))
                .endOffsets(asList(7, 15))
                .build();
    }
}