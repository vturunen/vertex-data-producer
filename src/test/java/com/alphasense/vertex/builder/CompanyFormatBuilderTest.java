package com.alphasense.vertex.builder;

import com.alphasense.vertex.SpringComponentTest;
import com.alphasense.vertex.dataobject.CompanyFormat;
import com.alphasense.vertex.dataobject.TickerData;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @author Niko Himanen
 *         Date: 09/29/2016.
 */
public class CompanyFormatBuilderTest extends SpringComponentTest {

    @Autowired
    private CompanyFormatBuilder builder;

    @Test
    public void build() throws Exception {
        String tickerId = "tickerId";
        String asId = "asId";
        String isin = "isin";
        String cusip = "cusip";
        String sedol = "sedol";
        String companyName = "companyName";
        CompanyFormat companyFormat = builder.build(
                new TickerData(
                        123L,
                        tickerId,
                        asId,
                        isin,
                        cusip,
                        sedol,
                        Boolean.TRUE,
                        Boolean.TRUE,
                        companyName
                )
        );

        assertEquals(tickerId, companyFormat.getAsTicker());
        assertEquals(asId, companyFormat.getAsId());
        assertEquals(isin, companyFormat.getIsin());
        assertEquals(cusip, companyFormat.getCusip());
        assertEquals(sedol, companyFormat.getSedol());
        assertEquals(companyName, companyFormat.getCompanyName());
    }
}