package com.alphasense.vertex.builder;

import com.alphasense.vertex.SpringComponentTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @author Niko Himanen
 *         Date: 09/29/2016.
 */
public class FileNameBuilderTest extends SpringComponentTest {

    @Autowired
    private FileNameBuilder builder;

    @Test
    public void build() throws Exception {
        String prefix = "prefix";

        String result = builder.build(prefix);

        assertTrue(result != null);

        String[] parts = result.split("/");

        assertEquals(2, parts.length);
        assertTrue(parts[0].split("-").length == 3);
        assertTrue(parts[1].startsWith(prefix + "_"));
    }
}