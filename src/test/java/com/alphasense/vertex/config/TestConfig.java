package com.alphasense.vertex.config;

import com.alphasense.vertex.builder.CompanyFormatBuilder;
import com.alphasense.vertex.builder.FileNameBuilder;
import com.alphasense.vertex.builder.VertexDocumentFormatBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;
import java.util.UUID;

/**
 * @author Niko Himanen
 *         Date: 09/26/2016.
 */

@Configuration
@EnableJpaRepositories(
        basePackages = "com.alphasense.vertex.repository",
        entityManagerFactoryRef = "beEntityManagerFactory",
        transactionManagerRef = "beTransactionManager"
)
@PropertySource({"classpath:application.properties"})
@EnableTransactionManagement
@EnableConfigurationProperties
public class TestConfig {

    @Bean
    public CompanyFormatBuilder companyFormatBuilder() {
        return new CompanyFormatBuilder();
    }

    @Bean
    public VertexDocumentFormatBuilder vertexFormatBuilder() {
        return new VertexDocumentFormatBuilder();
    }

    @Bean
    public FileNameBuilder fileNameBuilder() {
        return new FileNameBuilder();
    }

    @Bean
    @Primary
    @Qualifier("be")
    @ConfigurationProperties(prefix = "spring.datasource.be")
    public DataSource beDataSource() {
        final EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.addDefaultScripts();
        return builder.setName(UUID.randomUUID() + ";MODE=MySQL").setType(EmbeddedDatabaseType.H2).build();
    }

    @Bean
    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.setProperty("hibernate.show_sql", "false");
        return properties;
    }

    @Bean
    public PlatformTransactionManager beTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(beEntityManagerFactory().getObject());
        return transactionManager;
    }

    @Bean(name = "beEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean beEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(beDataSource());
        emf.setPackagesToScan("com.alphasense.vertex.dataobject");
        emf.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        emf.setJpaDialect(new HibernateJpaDialect());
        emf.setJpaProperties(additionalProperties());
        return emf;
    }
}