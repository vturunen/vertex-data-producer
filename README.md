This readme serves the purpose of a ChangeLog for the JSON format versions. Put latest version format on top.

JSON format v1.0
===

```
{
	"VERSION": "1.0",
	"KEYWORD": "CRISPR",
	"COPYRIGHT": "You are receiving this data subject to our standard <a href=\"http://www.alpha-sense.com/terms/#anchordisclaimer\" target=\"_blank\">Disclaimer</a>, <a href=\"http://www.alpha-sense.com/terms/#anchorterms\" target=\"_blank\">Terms of Service</a> and <a href=\"http://www.alpha-sense.com/privacy-policy/\" target=\"_blank\">Privacy Policy</a>.",
	"DOCUMENTS": [
		{
			"DOC_ID": "DOC_ID1",
			"TITLE": "TITLE1",
			"SOURCE": "SOURCE1",
			"RELEASED_DATE": "RELEASED_DATE1",
			"AVAILABILITY_DATE": "AVAILABILITY_DATE1",
			"TITLE_HIT_YN": "Y",
			"companies": [
				{
					"AS_TICKER": "TICKER1",
					"COMPANY_NAME": "Albany Molecular Research Inc",
					"ASID": "ASID1",
					"ISIN": "ISIN1",
					"CUSIP": "CUSIP1",
					"SEDOL": "SEDOL1"
				}
			],
			"snippets": [
				"Simon will take you through the numbers in more detail; and David will, importantly, take you through our <span class='hl'>capital position.</span>",
				"We have announced our Solvency ratio at June 30 at 134%, and the Board is comfortable with that ratio and its resilient <span class='hl'>capital position</span>, especially given our low level of gearing.",
				"So what we've done here is for each company calculate the Solvency II <span class='hl'>capital</span> coverage ratio using unrestricted Tier 1 <span class='hl'>capital</span>, and on this basis, the gold bar in the middle, you can see that our <span class='hl'>capital</span> position at 127% sits right in the middle of the pack."
			]
		},
		{
			"DOC_ID": "DOC_ID2",
			"TITLE": "TITLE2",
			"SOURCE": "SOURCE2",
			"RELEASED_DATE": "RELEASED_DATE2",
			"AVAILABILITY_DATE": "AVAILABILITY_DATE2",
			"TITLE_HIT_YN": "Y",
			"companies": [
				{
					"AS_TICKER": "TICKER2",
					"COMPANY_NAME": "Albany Molecular Research Inc",
					"ASID": "ASID2",
					"ISIN": "ISIN2",
					"CUSIP": "CUSIP2",
					"SEDOL": "SEDOL2"
				}
			],
			"snippets": [
				"Simon will take you through the numbers in more detail; and David will, importantly, take you through our <span class='hl'>capital position.</span>",
				"We have announced our Solvency ratio at June 30 at 134%, and the Board is comfortable with that ratio and its resilient <span class='hl'>capital position</span>, especially given our low level of gearing.",
				"So what we've done here is for each company calculate the Solvency II <span class='hl'>capital</span> coverage ratio using unrestricted Tier 1 <span class='hl'>capital</span>, and on this basis, the gold bar in the middle, you can see that our <span class='hl'>capital</span> position at 127% sits right in the middle of the pack."
			]
		}
	]
}
```